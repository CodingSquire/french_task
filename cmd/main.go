package main

import (
	"fmt"

	"study/french_task/pkg/generator"
)

func main() {
	svc := generator.NewService()

	//get 9 element
	fmt.Println(svc.GetConcrete(9))

	//get random element
	fmt.Println(svc.Next(221112221))
}
