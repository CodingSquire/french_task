package generator

import (
	"fmt"
	"strconv"
)

const startElement = 1

//Service - allows you to generate a "look-and-say" sequence
type Service interface {
	Start() (elem int)
	Next(elem int) (nextElem int)
	GetConcrete(position int) (elem int)
}

type service struct{}

//Start - returns def element
func (s *service) Start() (elem int) {
	return startElement
}

//GetConcrete - returns the element of a sequence by position
func (s *service) GetConcrete(position int) (elem int) {
	elem = s.Start()
	for i := 0; i < position-1; i++ {
		elem = s.Next(elem)
	}
	return elem
}

//Next - returns the next element of a sequence
func (s *service) Next(elem int) (nextElem int) {
	var (
		result string
		сount  int
	)
	strElem := strconv.Itoa(elem)
	lastElem := strElem[0]

	//parsing element
	for i := range strElem {
		if lastElem == strElem[i] {
			сount++
		} else {
			result = s.concatenations(result, сount, lastElem)
			lastElem = strElem[i]
			сount = 1
		}
	}
	result = s.concatenations(result, сount, lastElem)

	//converting to int
	nextElem, err := strconv.Atoi(result)
	if err != nil {
		fmt.Println("Got error:", err)
	}
	return nextElem
}

func (s *service) concatenations(
	result string,
	count int,
	element uint8,
) string {
	result = fmt.Sprintf("%s%d%c",
		result,
		count,
		element,
	)
	return result
}

//NewService ...
func NewService() Service {
	return &service{}
}
